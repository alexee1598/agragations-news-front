import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Res } from '../../core/model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private baseUrl = 'https://aggregation-news-back.herokuapp.com/';

  constructor(private http: HttpClient) {}

  getNews(): Observable<Res> {
    return this.http.get<Res>(this.baseUrl);
  }
}
