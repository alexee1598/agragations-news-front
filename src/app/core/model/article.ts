import DateTimeFormat = Intl.DateTimeFormat;

export class Article {
  id: number;
  news_site: number;
  url_address: string;
  title: string;
  description: string;
  created: DateTimeFormat;
  updated: DateTimeFormat;
  archived: boolean;
}

export class NewsSite {
  id: number;
  name: string;
  site: Article[];
}

export class Res {
  response: NewsSite[];
}
