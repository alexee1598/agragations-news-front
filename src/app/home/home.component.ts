import { Component, OnInit } from '@angular/core';
import { NewsSite } from '../core/model';
import { ApiService } from '../shared/services/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  response$: Observable<NewsSite[]>;

  constructor(private apiService: ApiService) {
    console.log('hretghe');
  }

  trackByFn(index: number): number {
    return index;
  }

  ngOnInit(): void {
    this.response$ = this.apiService
      .getNews()
      .pipe(map((value) => value.response));
    this.apiService.getNews().subscribe((value) => {
      console.log(value);
    });
  }
}
